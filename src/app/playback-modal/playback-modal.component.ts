import { Component } from '@angular/core';
import { SimpleModalComponent } from 'ngx-simple-modal';

export interface PlaybackModal {
  quote: object;
}

@Component({
  selector: 'app-playback-modal',
  templateUrl: './playback-modal.component.html',
  styleUrls: ['./playback-modal.component.scss']
})
export class PlaybackModalComponent extends SimpleModalComponent<PlaybackModal, object> implements PlaybackModal {
  
  quote: object;

  constructor() {
    super();
  }

  confirm() {
    //confirm and go to next step
    this.result = {isConfirmed: true};
    this.close();
  }
  
  cancel() {
    //they... cancel?
    this.result = {isConfirmed: false, goToStep: -1};
    this.close();
  }
  
  amend(target: number) {
    //give them somewhere to come back to
    this.result = {isConfirmed: false, goToStep: target};
    this.close();
  }

}
