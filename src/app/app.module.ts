import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { SimpleModalModule } from 'ngx-simple-modal';

import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { YourInformationComponent } from './your-information/your-information.component';
import { PlaybackModalComponent } from './playback-modal/playback-modal.component';
import { YourSummaryComponent } from './your-summary/your-summary.component';

@NgModule({
  declarations: [
    AppComponent,
    YourInformationComponent,
    PlaybackModalComponent,
    YourSummaryComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule,
    SimpleModalModule.forRoot({container: document.body}),
    RouterModule.forRoot([
    {path: 'your-information', component: YourInformationComponent},
    {path: '', redirectTo: '/your-information', pathMatch: 'full'},
  ]),
  ],
  entryComponents: [
    PlaybackModalComponent
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
