import { Component, OnInit } from '@angular/core';
import { Quote } from '../quote';
import { SimpleModalService } from 'ngx-simple-modal';
import { PlaybackModalComponent } from '../playback-modal/playback-modal.component';
import { trigger, state, style, animate, transition, query, group } from '@angular/animations';

@Component({
  selector: 'app-your-information',
  templateUrl: './your-information.component.html',
  styleUrls: ['./your-information.component.scss'],
  animations: [
    trigger( 'inOutAnimation', [
        transition(':increment', group([
          query(':enter', [
            style({ transform: 'translateX(100vw)', opacity: 0 }),
            animate('0.35s ease-in', style({ transform: 'translateX(0)', opacity: 1 }))
          ]),
          query(':leave', [
            style({ transform: 'translateX(0)', opacity: 1 }),
            animate('0.35s ease-out', style({ transform: 'translateX(-100vw)', opacity: 0 }))
          ])
        ])),
        transition(':decrement', group([
          query(':enter', [
            style({ transform: 'translateX(-100vw)', opacity: 0 }),
            animate('0.35s ease-in', style({ transform: 'translateX(0)', opacity: 1 }))
          ]),
          query(':leave', [
            style({ transform: 'translateX(0)', opacity: 1 }),
            animate('0.35s ease-out', style({ transform: 'translateX(100vw)', opacity: 0 }))
          ])
        ]))
    ])
  ]
})
export class YourInformationComponent implements OnInit {
  quote: Quote = {
    title: undefined,
    firstName: undefined,
    surname: undefined,
    postcode: undefined,
    address: undefined,
    monthlyPayment: undefined,
    coverLevel: undefined,
    startDate: {day: undefined, month: undefined, year: 2021},
    claims: undefined,
    noClaimsB: undefined,
    noClaimsC: undefined,
    jointPolicyHolder: undefined,
    jointTitle: undefined,
    jointFirstName: undefined,
    jointSurname: undefined,
    keepMeInformed: false,
    keepMeInformedByElectronicMeans: false,
    pawSquad: undefined,
    extantPolicy: undefined,
    carInsuranceMonth: undefined
  }
  
  
  days: Array<number> = Array.from({length: 31}, (_, i) => i + 1);
  
  months: Array<object> = [
    {value: 1, label: 'January'},
    {value: 2, label: 'February'},
    {value: 3, label: 'March'},
    {value: 4, label: 'April'},
    {value: 5, label: 'May'},
    {value: 6, label: 'June'},
    {value: 7, label: 'July'},
    {value: 8, label: 'August'},
    {value: 9, label: 'September'},
    {value: 10, label: 'October'},
    {value: 11, label: 'November'},
    {value: 12, label: 'December'}
  ];
  
  coverLevels: Array<object> = [
    {value: 'BC', label: 'Buildings and Contents'},
    {value: 'B', label: 'Building Only'},
    {value: 'C', label: 'Contents Only'}
  ];
  
  noClaimsValues: Array<object> = [
    {value: 0, label: '0'},
    {value: 1, label: '1'},
    {value: 2, label: '2'},
    {value: 3, label: '3'},
    {value: 4, label: '4'},
    {value: 5, label: '5+'}
  ];
  
  section: number = 0;
  
  noOfClaims: number = 0;
  
  showingAddress: boolean = false;
  showingClaims: boolean;
  showingNoClaims: boolean;
  
  showAddress() {
    !!this.quote.postcode && (this.showingAddress = true);
  }
  
  lastSection() {
    window.sessionStorage.setItem('dl-quickquote', JSON.stringify(this.quote));
    this.section--;
  }
  
  nextSection() {
    window.sessionStorage.setItem('dl-quickquote', JSON.stringify(this.quote));
    this.section++;
  }
  
  jumpToSection(section: number) {
    window.sessionStorage.setItem('dl-quickquote', JSON.stringify(this.quote));
    this.section = section;
  }
  
  playbackModal () {
    window.sessionStorage.setItem('dl-quickquote', JSON.stringify(this.quote));
    this.SimpleModalService.addModal(PlaybackModalComponent, {
      quote: this.quote})
      .subscribe((results : any) => {
        if (results.isConfirmed) {
          //GO TO NEXT STEP
          alert('CRAPPO BAPPO');
        } else if (results.goToStep != undefined && results.goToStep > -1) {
          this.jumpToSection(results.goToStep);
        }
    });
  }
  
  updateMonthlyPayment(value: boolean) {
    this.quote.monthlyPayment = value;
  }
  
  updateCoverLevel(value: string) {
    this.quote.coverLevel = value;
  }
  
  toggleClaims(value: boolean) {
    this.quote.claims = [];
    this.showingClaims = value;
  }
  
  toggleJoint(value: boolean) {
    this.quote.jointPolicyHolder = value;
  }
  
  togglePaws(value: boolean) {
    this.quote.pawSquad = value;
  }
  
  toggleExtant(value: boolean) {
    this.quote.extantPolicy = value;
  }
  
  updateClaimsNumber(value: number) {
    value < 0 && (value = 0); //don't enter negative claims
    this.noOfClaims = value;
    
    //only handle if it's under 6 - if it's more, it's an error
    if (value < 6) {
      //if it's too short, push more empty claims objects
      while (this.quote.claims.length < value) {
        this.quote.claims.push({}); //actual object needs adding here
      }
      
      //if it's too long, chop some claims off the end
      this.quote.claims.length > value && (this.quote.claims.length = value);
    }
  }
  
  toggleNoClaims(value: boolean) {
    this.quote.noClaimsB = value ? undefined : 0;
    this.quote.noClaimsC = value ? undefined : 0;
    this.showingNoClaims = value;
  }

  constructor(private SimpleModalService: SimpleModalService) {}

  ngOnInit() {
    this.quote = JSON.parse(window.sessionStorage.getItem('dl-quickquote')) || this.quote;
    
    this.quote.postcode != undefined && (this.showingAddress = !!this.quote.postcode);
    this.quote.claims != undefined && (this.showingClaims = !!this.quote.claims.length);
    if (this.quote.noClaimsB != undefined || this.quote.noClaimsC != undefined) {
      this.showingNoClaims = (!!this.quote.noClaimsB && !!this.quote.noClaimsC);
    } 
  }

}
