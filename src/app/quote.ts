export interface Quote {
  title: string;
  firstName: string;
  surname: string;
  postcode: string;
  address: string;
  monthlyPayment: boolean;
  coverLevel: string;
  startDate: object;
  claims: Array<object>;
  noClaimsB: number;
  noClaimsC: number;
  jointPolicyHolder: boolean;
  jointTitle: string;
  jointFirstName: string;
  jointSurname: string;
  keepMeInformed: boolean;
  keepMeInformedByElectronicMeans: boolean;
  pawSquad: boolean;
  extantPolicy: boolean;
  carInsuranceMonth: number;
}